"use strict";

class Actor {
    constructor(x, y) {
        this._x = x;
        this._y = y;
        this._gameBox = $('.gameBox');
    }

    get x() {
        let gameBoxWidth = this._gameBox.width() - 60;
        this._x = (this._x > gameBoxWidth) ? gameBoxWidth : (this._x < 10) ? 10 : this._x;
        return this._x;
    };

    get y() {
        let gameBoxHeight = this._gameBox.height() - 60;
        this._y = (this._y > gameBoxHeight) ? gameBoxHeight : (this._y < 10) ? 10 : this._y;
        return this._y;
    };

    move(x, y) {
        this._x += x;
        this._y += y;
    }
}