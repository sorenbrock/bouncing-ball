/* Generated by Babel */
"use strict";

var _createClass = (function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; })();

var _get = function get(_x, _x2, _x3) { var _again = true; _function: while (_again) { var object = _x, property = _x2, receiver = _x3; desc = parent = getter = undefined; _again = false; if (object === null) object = Function.prototype; var desc = Object.getOwnPropertyDescriptor(object, property); if (desc === undefined) { var parent = Object.getPrototypeOf(object); if (parent === null) { return undefined; } else { _x = parent; _x2 = property; _x3 = receiver; _again = true; continue _function; } } else if ("value" in desc) { return desc.value; } else { var getter = desc.get; if (getter === undefined) { return undefined; } return getter.call(receiver); } } };

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function, not " + typeof superClass); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, enumerable: false, writable: true, configurable: true } }); if (superClass) Object.setPrototypeOf ? Object.setPrototypeOf(subClass, superClass) : subClass.__proto__ = superClass; }

var Enemy = (function (_Actor) {
    _inherits(Enemy, _Actor);

    function Enemy(x, y) {
        _classCallCheck(this, Enemy);

        _get(Object.getPrototypeOf(Enemy.prototype), "constructor", this).call(this, x, y);
        var speedX = this.randomNumber(1, 6);
        var speedY = this.randomNumber(1, 6);
        this._moveX = Boolean(this.randomNumber(0, 1)) ? -speedX : speedX;
        this._moveY = Boolean(this.randomNumber(0, 1)) ? -speedY : speedY;
        this._gameBox = $('.gameBox');
    }

    _createClass(Enemy, [{
        key: "makeMove",
        value: function makeMove() {
            var gameBoxWidth = this._gameBox.width() - 60;
            var gameBoxHeight = this._gameBox.height() - 60;
            this._moveX = this.x + this._moveX <= 10 || this.x + this._moveX >= gameBoxWidth ? -this._moveX : this._moveX;
            this._moveY = this.y <= 10 || this.y >= gameBoxHeight ? -this._moveY : this._moveY;
            this.move(this._moveX, this._moveY);
        }
    }, {
        key: "randomNumber",
        value: function randomNumber(min, max) {
            return Math.round(Math.random() * (max - min) + min);
        }
    }]);

    return Enemy;
})(Actor);