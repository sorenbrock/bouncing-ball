"use strict";

class Enemy extends Actor {
    constructor(x, y) {
        super(x, y);
        let speedX = this.randomNumber(1, 6);
        let speedY = this.randomNumber(1, 6);
        this._moveX = Boolean(this.randomNumber(0, 1)) ? -speedX : speedX;
        this._moveY = Boolean(this.randomNumber(0, 1)) ? -speedY : speedY;
        this._gameBox = $('.gameBox');
    }

    makeMove() {
        let gameBoxWidth = this._gameBox.width() - 60;
        let gameBoxHeight = this._gameBox.height() - 60;
        this._moveX = (this.x + this._moveX <= 10 || this.x + this._moveX >= gameBoxWidth) ? -this._moveX : this._moveX
        this._moveY = (this.y <= 10 || this.y >= gameBoxHeight) ? -this._moveY : this._moveY
        this.move(this._moveX, this._moveY);
    }

    randomNumber(min, max) {
        return Math.round((Math.random() * (max - min)) + min);
    }
}